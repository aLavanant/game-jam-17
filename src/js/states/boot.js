var bootState = {
	create : function(){
		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.input.gamepad.start();
		this.state.start('load');
	}
}


export default bootState;