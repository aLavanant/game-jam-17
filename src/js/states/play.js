var WALLS= [
	{
		x: 2,
		y : 0
	}, {
		x: 4,
		y : 0
	}, {
		x: 14,
		y : 0
	},

	{
		x: 6,
		y : 2
	}, {
		x: 12,
		y : 2
	}, {
		x: 14,
		y : 2
	},

	{
		x: 7,
		y : 4
	},

	 {
		x: 0,
		y : 6
	}, {
		x: 2,
		y : 6
	}, {
		x: 8,
		y : 6
	},

	 {
		x: 0,
		y : 8
	}, {
		x: 10,
		y : 8
	}, {
		x: 12,
		y : 8
	}
];


var LAVA= [{
	x: 0,
	y : 0,
	w : 2
}, {
	x: 5,
	y : 0,
	w : 9
}, {
	x: 0,
	y : 1,
	h : 5
},{
	x: 4,
	y : 2,
	w : 2
},{
	x: 8,
	y : 2,
	w : 3
},


{
	x: 2,
	y : 3,
	h : 3
},{
	x: 4,
	y : 3
},{
	x: 10,
	y : 3
},{
	x: 12,
	y : 3,
	h : 3
},{
	x: 14,
	y : 3,
	h : 5
},

{
	x: 4,
	y : 5
},{
	x: 10,
	y : 5
},

{
	x: 4,
	y : 6,
	w : 3
},{
	x: 9,
	y : 6,
	w : 2
},{
	x: 1,
	y : 8,
	w : 9
}, {
	x: 13,
	y : 8,
	w : 2
}
];




var playState = {
	init : function(options){
		this.players = options.players;
		this.nbPlayers =0;
	},

	preload : function(){
		for (var i = 0; i< 4; ++i){
			this.load.image('ground_'+i, './src/assets/img/env/ground_64_0'+i+'.png');
		}
	},

	create : function(){

		// build map
		
		var map = this.add.tilemap();
		for (var i = 0; i< 4; ++i){
			map.addTilesetImage('tile_ground_' + i, 'ground_' + i, 64, 64, 0, 0, i);
		}

		var layer_bottom = map.create('bottom', 15, 9, 64, 64);
		map.fill(1, 0, 0, 15, 9);
		layer_bottom.resizeWorld();

			
		this.walls = map.createBlankLayer('walls', 15, 9, 64, 64);
		this.fillTiles(map, WALLS, this.walls, 0);

		
		this.lava = map.createBlankLayer('lava', 15, 9, 64, 64);
		this.fillTiles(map, LAVA, this.lava, 2);

		//players !!!!

		this.playerGroup = this.add.physicsGroup();

		for (var i in this.players){
			this.nbPlayers++;
			var p = this.players[i];
			this.add.existing(p.sprite);
			p.sprite.animations.play(p.animation);
			p.sprite.scale.x = 0.4;
			p.sprite.scale.y = 0.4;

			this.playerGroup.add(p.sprite);
		}

		this.playerGroup.setAll('body.collideWorldBounds', true);
    	this.playerGroup.setAll('body.bounce.x', 1);
    	this.playerGroup.setAll('body.bounce.y', 1);


	},
	update : function(){
		this.physics.arcade.collide(this.playerGroup);





		for (var i in this.players){
			var p = this.players[i];
			this.physics.arcade.collide(p.sprite, this.walls);
			this.physics.arcade.overlap(p.sprite, this.lavaGroup, this.playerDies);

			this.controls(p.sprite, p.pad);
		}
	},

	playerDies : function(){
		console.log(arguments)
	},	

	fillTiles : function(map, array, layer, tileIdx){
		for (var i = 0; i < array.length; ++i){
			var width = array[i].w || 1;
			var height = array[i].h || 1;
			map.fill(tileIdx, array[i].x, array[i].y, width,  height, layer);
		}
	},


	handleCollision : function(){
		console.log(arguments)
	},

	controls(sprite, pad){
		// Controls
		var vx = sprite.body.velocity.x;
		var vy = sprite.body.velocity.y;
		var ax = sprite.body.acceleration.x;
		var ay = sprite.body.acceleration.y;

		var axisX = pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X);
		sprite.body.velocity.x = this.computeVel(vx, axisX);
		var axisY = pad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y);
		sprite.body.velocity.y = this.computeVel(vy, axisY);

		if (pad.justPressed(Phaser.Gamepad.XBOX360_A))
	    {
	        
	    }

	    if (pad.justReleased(Phaser.Gamepad.XBOX360_B))
	    {
	        
	    }
	},


	computeVel: function(v, axis){
		var res = 0;
		var factor = this.time.physicsElapsed * 2000;
		if (!!axis && Math.abs(axis) > 0.1){
			var delta = factor * axis;
			if (v !== 0 && Math.sign(v)*Math.sign(delta) < 0){
				delta *= 2;
			}
			res = v + delta;
			res = Math.min(500, Math.abs(res))* Math.sign(res);
		} else if (Math.abs(v) > 0){
			var delta = factor * 1.5;
			res = Math.max(0, Math.abs(v) - delta)*Math.sign(v);
		}
		return res;
	}
}


export default playState;