const CHAR = ["girl", "geek", "croco", "old", "misterT", "flamingo"];
const GROUND = ["ground", "lava", "wall"];
const COLORS = ["red", "blue", "purple", "green", "dead"];


var loadState = {
	preload : function(){
		for (var i in COLORS){
        	var name = 'chars_' + COLORS[i];
        	this.load.spritesheet(name, './src/assets/img/char/' + name + '.png', 162, 162);
      	}

	    this.load.audio('intro', ['./src/assets/sound/BRUTALBRAWL_intro_menus.mp3'])
	    this.load.audio('choose', ['./src/assets/sound/CHOOSEYOURCHARACTER_.mp3'])
	},
	create : function(){

		var nbPlayers = 0;
		var players = {};

		for (var i = 0; i < 4; ++i){

			var pad = this.input.gamepad['pad' + (i+1)];
			if (!pad.connected){
				break;
			}

			nbPlayers++;

			players[i] = {
				pad : pad,
				color : COLORS[i],
				char : CHAR[i],
				char_idx : i,
				sprite : this.createCharSprites(COLORS[i]),
				spriteDead : this.createCharSprites("dead"),
				animation : CHAR[i]+ '_down'
			};
		}


		var nbMonsters = 3;

		this.state.start('menu', true, false, {
			nbPlayers : nbPlayers,
			players : players
		});

		var music = this.add.audio('intro');
		var chooseFX = this.add.audio('choose');
		music.volume = 0.3;
    	music.play();
    	chooseFX.play();
	},

	createCharSprites : function(color){

		var sprite = this.make.sprite(0, 0, 'chars_' + color);
		this.physics.enable(sprite, Phaser.Physics.ARCADE);

		for (var i in CHAR){
        	var name = CHAR[i];
			sprite.animations.add(name +'_up', [i * 4 + 3], 10, true);
			sprite.animations.add(name +'_side', [i * 4 + 1, i * 4 + 2], 10, true);
			sprite.animations.add(name +'_down', [i * 4 + 0], 10, true);
		}
		sprite.anchor.set(.5);
		sprite.body.setSize(100, 150, 31, 21);

		return sprite;
	},
	createMonsterSprites : function(name){
		var sprite = this.make.sprite(0, 0, name);
		sprite.anchor.set(.5);
		for (var n in ["bear", "bison"]){
			sprite.animations.add(n +'_up', [n * 4 + 3], 10, true);
			sprite.animations.add(n +'_side', [n * 4 + 1, n * 4 + 2], 10, true);
			sprite.animations.add(n +'_down', [n * 4 + 0], 10, true);
		}
		return sprite;
	}
}

export default loadState;