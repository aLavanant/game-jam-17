const COLORS = ["red", "blue", "purple", "green", "dead"];
const CHAR = ["girl", "geek", "croco", "old", "misterT", "flamingo"];


var menuState = {
	init : function(options){
		this.nbPlayers = options.nbPlayers;
		this.players = options.players;
	},

	create : function(){
		this.add.text(100, 100, 'MENU', {fill : "#ffffff"});

		if (!this.nbPlayers){
			this.add.text(100, 600, 'NO Gamepad connected', {fill : "#ffffff"});
		}

		var colWidth = this.game.width /this.nbPlayers;

		for (var i = 0; i < this.nbPlayers; ++i){
			var p = this.players[i];
			p.x = (i + 0.5) * colWidth;
			p.y = this.game.height/2;

			this.add.existing(p.sprite);
			p.sprite.x = p.x;
			p.sprite.y = p.y

			p.sprite.animations.play(p.animation);
		}

		
		this.counter = 1;
		this.timer = Date.now();
		this.countdown = this.add.text(this.game.width/2, this.game.height - this.game.height*0.1, this.counter, {fill : "#ff0000"});
		this.countdown.anchor.set(.5);

	},

	update : function(){

		//counter
		if (Date.now() - this.timer > 1000){
			this.counter --;
			this.timer = Date.now();
			if (this.counter < 0){
				this.state.start('play', true, false, {
					players : this.players
				});
        		return;
			} else {
				this.countdown.text = this.counter || 'GO !';
			}
		}


		for (var i in this.players){
			var p = this.players[i];

			if (p.pad.justPressed(Phaser.Gamepad.XBOX360_A, 500) && !p.isPressed) {
				p.char_idx = (p.char_idx+1)%CHAR.length;

				var name = CHAR[p.char_idx];
				p.animation = name + '_down';
				p.sprite.animations.play(p.animation);

				p.isPressed = true;
    		} else if (p.pad.justReleased(Phaser.Gamepad.XBOX360_A) && p.isPressed){
    			p.isPressed = false;
    		}
		}
	},

	shutdown : function() {
		for (var i in this.players){
  			this.world.remove(this.players[i].sprite);
  		}
	}
}


export default menuState;