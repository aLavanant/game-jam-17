import Vue from 'vue'
import App from './js/App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
